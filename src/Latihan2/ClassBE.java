package Latihan2;

public class ClassBE {
    public String nama;
    public int umur;
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }


    public String m_judul() {

        return "Class BE Method final Anggota";
    }

    public String m_anggota(String nama, int umur ) {
        this.nama = nama;
        this.umur = umur;
        System.out.println("Method m_anggota pada ClassBE");
        return "nama= "+getNama()+"umur= "+getUmur();

    }
}
